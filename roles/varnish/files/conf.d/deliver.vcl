vcl 4.0;

sub vcl_deliver {
    if (resp.http.X-Varnish-Cacheable) {
        # set resp.http.Cache-Control = "max-age=60, public";
        # set resp.http.Cache-Control = "public";
        # set resp.http.Cache-Control = "private";
        set resp.http.Cache-Control = "no-cache";
    }

    # Please use this for debugging purpose only
    #if (obj.hits > 0) {
    #    set resp.http.X-Cache = "HIT";
    #    set resp.http.X-Cache-Hits = obj.hits;
    #    set resp.http.X-Cache-TTL-Remaining = req.http.X-Cache-TTL-Remaining;
    #    set resp.http.X-Cache-Age = req.http.X-Cache-Age;
    #    set resp.http.X-Cache-Grace = req.http.X-Cache-Grace;
    #} else {
    #    set resp.http.X-Cache = "MISS";
    #}

    unset resp.http.V-HOST;
    unset resp.http.V-URL;

    # Remove PHP , Apache and Varnish versions
    unset resp.http.X-Powered-By;
    unset resp.http.Server;
    unset resp.http.X-Varnish;
    unset resp.http.Via;
    unset resp.http.Link;
    unset resp.http.X-Generator;

    unset resp.http.Surrogate-Control;
    unset resp.http.X-Varnish-Cacheable;
    unset resp.http.X-Varnish-Uncacheable;
}
