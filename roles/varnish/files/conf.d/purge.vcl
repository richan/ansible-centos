vcl 4.0;

sub vcl_purge {
    # Only handle actual PURGE HTTP methods, everything else is discarded
    if (req.method == "PURGE") {
        set req.http.X-Cache-Purge = "Yes";
        return (restart);
    }
}
