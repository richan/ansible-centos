vcl 4.0;

acl purge_list {
  # ACL we'll use later to allow purges
  "localhost";
  "127.0.0.1";
  "::1";
}

acl ban_list {
  # ACL we'll use later to allow bans
  "localhost";
  "127.0.0.1";
  "::1";
}
