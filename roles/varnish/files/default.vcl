vcl 4.0;

import directors;
import std;
#import header;

include "snippets/clean_ga_cookies.vcl";
include "snippets/clean_third_party_cookies.vcl";
include "snippets/handle_backend_response.vcl";
include "snippets/normalize_accept_encoding.vcl";
include "snippets/normalize_request.vcl";

include "conf.d/acl.vcl";
include "conf.d/backend_fetch.vcl";
include "conf.d/backend_response.vcl";
include "conf.d/backends.vcl";
include "conf.d/deliver.vcl";
#include "conf.d/error.vcl";
include "conf.d/hash.vcl";
include "conf.d/hit.vcl";
include "conf.d/init.vcl";
include "conf.d/purge.vcl";
include "conf.d/recv.vcl";
