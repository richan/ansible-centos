vcl 4.0;

sub normalize_accept_encoding {
    if(req.http.Accept-Encoding) {
        if(req.url ~ "\.(jpg|png|gif|gz|tgz|bz2|tbz|mp[34]|ogg)$") {
            # No point compressing these extensions
            unset req.http.Accept-Encoding;
        } elsif(req.http.Accept-Encoding ~ "gzip") {
            set req.http.Accept-Encoding = "gzip";
        } elsif(req.http.Accept-Encoding ~ "deflate") {
            set req.http.Accept-Encoding = "deflate";
        } elsif(req.http.Accept-Encoding ~ "sdch") {
            set req.http.Accept-Encoding = "sdch";
        } else {
            # Unknown Algorithm
            unset req.http.Accept-Encoding;
        }
    }
}
