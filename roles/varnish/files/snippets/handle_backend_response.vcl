vcl 4.0;

sub handle_backend_response {
    # Allow stale content, in case the backend goes down.
    # make Varnish keep all objects for 12 hours beyond their TTL
    set beresp.grace = 12h;

    # Set additional HTTP headers for banning purposes
    set beresp.http.V-HOST = bereq.http.host;
    set beresp.http.V-URL = bereq.url;

    # Do ESI if the backend does acknowledge ESI support
    if (beresp.http.Surrogate-Control ~ "ESI/1.0") {
        unset beresp.http.Surrogate-Control;
        set beresp.do_esi = true;
    }

    # Enable cache for all static files
    if (bereq.url ~ "^[^?]*\.(7z|avi|bmp|bz2|css|csv|doc|docx|eot|flac|flv|gif|gz|ico|jpeg|jpg|js|less|mka|mkv|mov|mp3|mp4|mpeg|mpg|odt|otf|ogg|ogm|opus|pdf|png|ppt|pptx|rar|rtf|svg|svgz|swf|tar|tbz|tgz|ttf|txt|txz|wav|webm|webp|woff|woff2|xls|xlsx|xml|xz|zip)(\?.*)?$") {
        unset beresp.http.set-cookie;
    }

    # Large static files are delivered directly to the end-user
    if (bereq.url ~ "^[^?]*\.(7z|avi|bz2|flac|flv|gz|mka|mkv|mov|mp3|mp4|mpeg|mpg|ogg|ogm|opus|rar|tar|tgz|tbz|txz|wav|webm|xz|zip)(\?.*)?$") {
        unset beresp.http.set-cookie;
        set beresp.do_stream = true;
    }

    # Remove custom http / https port
    if (beresp.status == 301 || beresp.status == 302) {
        set beresp.http.Location = regsub(beresp.http.Location, ":[0-9]+", "");
    }

    # Don't cache 50x responses
    if (beresp.status == 500 || beresp.status == 502 || beresp.status == 503 || beresp.status == 504) {
        return (abandon);
    }

    # Cache 404 pages only for a small amount of time
    if (beresp.status == 404) {
        unset beresp.http.set-cookie;
        unset beresp.http.cache-control;
        set beresp.uncacheable = false;
        set beresp.http.Cache-Control = "public, max-age=60";
        set beresp.ttl = 60s;

        return (deliver);
    }
}
