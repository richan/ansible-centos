vcl 4.0;

sub normalize_request {
    # Remove anchor or hash references
    if (req.url ~ "\#") {
        set req.url = regsub(req.url, "\#.*$", "");
    }

    # Strip a trailing ? if it exists
    if (req.url ~ "\?$") {
        set req.url = regsub(req.url, "\?$", "");
    }

    # Normalize the header, remove the port (in case you're testing this on various TCP ports)
    set req.http.Host = regsub(req.http.Host, ":[0-9]+", "");

    # Remove the proxy header (see https://httpoxy.org/#mitigate-varnish)
    unset req.http.proxy;

    # Clean Google analytics related parameters and cookies
    call clean_ga_cookies;

    # Clean any third party cookies
    call clean_third_party_cookies;

    # Normalize query arguments by sorting
    set req.url = std.querysort(req.url);

    # Normalize HTTP Accept-Encoding header
    call normalize_accept_encoding;

    # Announce ESI Support
    set req.http.Surrogate-Capability = "abc=ESI/1.0";

    # Allow only specific IP Addresses to send any PURGE request
    if (req.method == "PURGE") {
        if (!client.ip ~ purge_list) {
            return (synth(405, "This IP is not allowed to send PURGE requests."));
        }
        return (purge);
    }

    # Allow only specific IP Addresses to send any BAN request
    if (req.method == "BAN") {
        if (!client.ip ~ ban_list) {
            return (synth(405, "This IP is not allowed to send BAN requests."));
        }

        if (req.http.x-ban-regex) {
            ban("obj.http.X-Varnish-Cacheable == 1 && obj.http.V-HOST == " + req.http.x-ban-host + " && obj.http.V-URL ~ " + req.http.x-ban-regex);
        }
        if (req.http.x-ban-url) {
            ban("obj.http.X-Varnish-Cacheable == 1 && obj.http.V-HOST == " + req.http.x-ban-host + " && obj.http.V-URL == " + req.http.x-ban-url);
        }
        return (synth(200, "Your BAN has successfully added."));
    }

    if (req.method == "FULLBAN") {
        if (!client.ip ~ ban_list) {
            return (synth(405, "This IP is not allowed to send BAN requests."));
        }

        ban("obj.http.X-Varnish-Cacheable == 1 && obj.http.V-HOST == " + req.http.x-ban-host + " && obj.http.V-URL ~ .*");
        return (synth(200, "Your FULLBAN has successfully added."));
    }

    # Only deal with normal HTTP requests
    if (req.method != "GET" &&
        req.method != "HEAD" &&
        req.method != "PUT" &&
        req.method != "POST" &&
        req.method != "TRACE" &&
        req.method != "OPTIONS" &&
        req.method != "PATCH" &&
        req.method != "DELETE") {
            return (synth(404, "Non-valid HTTP method!"));
    }

    # Only cache GET or HEAD requests. This makes sure the POST requests are always passed.
    if (req.method != "GET" && req.method != "HEAD") {
        return (pass);
    }

    # Large static files are delivered directly to the end-user
    if (req.url ~ "^[^?]*\.(7z|avi|bz2|flac|flv|gz|mka|mkv|mov|mp3|mp4|mpeg|mpg|ogg|ogm|opus|rar|tar|tgz|tbz|txz|wav|webm|xz|zip)(\?.*)?$") {
        unset req.http.Cookie;
        return (hash);
    }

    # Remove all cookies for static files
    if (req.url ~ "^[^?]*\.(7z|avi|bmp|bz2|css|csv|doc|docx|eot|flac|flv|gif|gz|ico|jpeg|jpg|js|less|mka|mkv|mov|mp3|mp4|mpeg|mpg|odt|otf|ogg|ogm|opus|pdf|png|ppt|pptx|rar|rtf|svg|svgz|swf|tar|tbz|tgz|ttf|txt|txz|wav|webm|webp|woff|woff2|xls|xlsx|xml|xz|zip)(\?.*)?$") {
        unset req.http.Cookie;
        return (hash);
    }
}
