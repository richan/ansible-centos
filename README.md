# Ansible CentOS #

A collection of ansible playbooks for CentOS or RHEL Linux distributions

> The ansible playbooks in this repository has been tested with ansible 2.9.6

## Table of contents

* [CentOS Version Compatibility](#centos-version-compatibility)
* [Virtual Machine Preparation](#virtual-machine-preparation)
* [Local Environment Preparation](#local-environment-preparation)
* [Basic usage](#basic-usage)
* [Installed softwares](#installed-softwares)
* [License](#license)

## CentOS Version Compatibility

| CentOS Distribution | Branch Name |
| ------------------- | ----------- |
| CentOS 7            | master      |

## Virtual Machine Preparation

### Install the required CentOS packages / libraries

First, you need to login to the CentOS virtual machine as root and then enter the commands below:

```bash
$ sudo yum -y update
$ sudo yum -y upgrade
$ sudo yum install vim nano python3
```

### Create a new user with sudo capability

To add a new sudo user, you need to login to the CentOS virtual machine as root and enter the command:

```bash
$ adduser username
```

Next, create a very strong password for the new user by entering the following in your terminal window:

```bash
$ passwd username
```

To add the new user as a sudoers, you need to enter this command:

```bash
$ usermod –aG wheel username
```

### Make the new user can perform sudo commands without prompting any password

Open and edit the visudo configuration:

```bash
$ sudo visudo
```

Then add this line at the bottom of the `visudo` configuration:

```config
username ALL=(ALL) NOPASSWD: ALL
```

### Register your public SSH key to login as the new user

```bash
$ cd /home/username
$ mkdir .ssh
$ vim .ssh/authorized_keys
```

After you've done copy-pasting the SSH public key and saved the file, you need to make sure the directory and file have a correct permission

```bash
$ chmod 0700 .ssh
$ chmod 0600 .ssh/authorized_keys
$ chown -R username:username .ssh
```

## Local Environment Preparation

* Make sure you have installed ansible version `2.9.6` or above.
* Please update the ansible inventory file : ``/hosts``
* Please update the variables in ``vars/resource_allocation.yml``
* If it's necessary, you can also update the variables in ``vars/global.yml``

## Basic usage

``ansible-playbook -i hosts <role>.yml``

This is the list of roles available in current package :
* common
* nginx
* php
* mysql
* memcached
* varnish
* _~~redis~~_

### Completing MySQL Installation

It is unfortunate, but currently there is no way to fully automate the MySQL installation, because at the end of the installation process MySQL will automatically set a temporary password for the root user.

In order to complete the installation, you need to execute these steps manually as soon as the installation had finished.

#### Retrieve the temporary password for the root user

```bash
$ sudo grep 'temporary password' /var/log/mysqld.log
```

#### Update the root password

```bash
$ mysql -u root -p mysql
```

```sql
ALTER USER 'root'@'localhost' IDENTIFIED BY 'V3ry-Str0ng-Passw0rd';
```

#### Secure MySQL Installation

```bash
$ sudo mysql_secure_installation
```

## Installed softwares

| Software   | Version |
| ---------- | ------- |
| nginx      | 1.16.1  |
| MySQL      | 8.0.21  |
| PHP        | 7.4.9   |
| memcached  | 1.4.15  |
| varnish    | 6.0.6   |
| phpmyadmin | 5.0.2   |

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.
